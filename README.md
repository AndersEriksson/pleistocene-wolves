Code for population genetic simulations of Pleistocene wolves.

Reference: Loog et al. (2019) Modern wolves trace their origin to a late Pleistocene expansion from Beringia. Molecular Ecology (to appear).